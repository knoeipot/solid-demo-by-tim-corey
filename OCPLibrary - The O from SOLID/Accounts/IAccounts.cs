﻿namespace OCPLibrary___The_O_from_SOLID
{
    public interface IAccounts
    {
        EmployeeModel Create(IApplicantModel person);
    }
}