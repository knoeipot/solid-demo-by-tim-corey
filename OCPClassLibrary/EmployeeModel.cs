﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OCPClassLibrary
{
    public class EmployeeModel : PersonModel
    {
        public string EmailAddress { get; set; }

        public bool IsManager { get; set; } = false;

        public bool IsExecutive { get; set; } = false;
    }
}
