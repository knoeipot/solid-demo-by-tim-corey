﻿using System;
using Unity;

namespace DependencyInjectionConsoleUI
{
    // UI Layer
    class Program
    {
        static void Main(string[] args)
        {

            IUnityContainer objContainer = new UnityContainer();
            objContainer.RegisterType<Customer2>();
            objContainer.RegisterType<IDal, SQLServerDAL>();
            objContainer.RegisterType<IDal, OracleDAL>();

            Customer2 obj = objContainer.Resolve<Customer2>();

            /*
            Customer obj = new Customer(); // Tight
            Customer2 obj2 = new Customer2(new SQLServerDAL()); // Loose
            */
            obj.Name = "Test 1";
            obj.Add();
        }
    }

    // Middle/Model Layer
    public class Customer // Tight
    {
        private OracleDAL OracleDAL = new OracleDAL();
        private SQLServerDAL Odal = new SQLServerDAL();

        // Tight Coupling Example
        public void Add()
        {
            if (true) // From config, parameter, ...
            {
                Odal.Add();
            }
            else
            {
                OracleDAL.Add();
            }


            Odal.Add();
        }

        public string  Name{ get; set; }
    }

    public class Customer2 //Loose
    {
        private IDal Odal;
        public string Name { get; set; }

        public void Add()
        {
            /*
            if(true)
            {
                Odal = new SQLServerDAL(); // Tight - Object Creation
            }
            else
            {
                Odal = new OracleDAL();
            }
            */
            Odal.Add();

        }
        public Customer2(IDal iobj)
        {
            Odal = iobj;
        }
    }


    // The bridge between Tight Coupling and Loosly Coupling
    // is always an Interface
    public interface IDal
    {
        void Add();
    }




    // DAL - Data Access Layer
    // Old/Tight Coupling Situation
    // New/Loosly Situation is inheriting the interface
    // SQL
    public class SQLServerDAL : IDal
    {
        public void Add()
        {

        }
    }

    // Oracle
    public class OracleDAL :IDal
    {
        public void Add()
        {

        }
    }
}
