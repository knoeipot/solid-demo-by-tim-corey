﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Console_UI___SOLID_03

{
    public class PersonValidator
    {
        public static bool Validate(Person person)
        {
            // Checks to be sure the first and last name are valid
            if (string.IsNullOrWhiteSpace(person.FirstName))
            {
                ////Console.WriteLine("You did not give us a valid first name");
                ////Console.ReadLine();
                ////StandardMessages.EndApplication();
                StandardMessages.DisplayValidationError("first name");
                return false;
            }

            if (string.IsNullOrWhiteSpace(person.LastName))
            {
                ////Console.WriteLine("You did not give us a valid last name");
                ////Console.ReadLine();
                ////StandardMessages.EndApplication();
                StandardMessages.DisplayValidationError("last name");
                return false;
            }

            return true;
        }
    }
}
