﻿namespace DependencyInjectionAutofacLibrary
{
    public interface IBusinessLogic
    {
        void ProcessData();
    }
}