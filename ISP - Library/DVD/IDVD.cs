﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISP___Library
{
    public interface IDVD
    {
        List<string> Actors { get; set; }

        int RunTimeInMinutes { get; set; }
    }
}
