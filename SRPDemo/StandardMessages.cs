﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRPDemo
{
    public class StandardMessages
    {
        public static void WelcomeMessage()
        {
            Console.WriteLine("Welcome to my application!");
        }

        public static void EndApplication()
        {
            Console.Write("Press enter to close...");
            Console.ReadLine();
        }

        public static void DisplayValidationError(string fieldname)
        {
            Console.WriteLine($"You did not give us a valid {fieldname}");
        }
    }
}
