﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Console_UI___SOLID_03
{
    public class Person
    {

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

    }
}
