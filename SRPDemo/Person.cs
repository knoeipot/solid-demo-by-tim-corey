﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRPDemo
{
    public class Person
    {

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

    }
}
