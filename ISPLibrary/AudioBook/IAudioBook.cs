﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISPLibrary
{
    public interface IAudioBook : ILibraryItem
    {
        int RuntimeInMinutes { get; set; }
    }
}
