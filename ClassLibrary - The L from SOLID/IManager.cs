﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary___The_L_from_SOLID
{
    public interface IManager : IEmployee
    {
        void GeneratePerformanceReview();
    }
}
