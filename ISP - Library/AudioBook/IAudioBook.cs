﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISP___Library
{
    public interface IAudioBook : ILibraryItem
    {
        public int RunTimeInMinutes { get; set; }
    }
}
