﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISP___Library
{
    public interface IBook : ILibraryItem
    {
        string Author { get; set; }
        //DateTime BorrowDate { get; set; }
        //string Borrower { get; set; }
        //int CheckOutDurationInDays { get; set; }
        int Pages { get; set; }
    }
}
