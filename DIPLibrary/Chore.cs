﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIPLibrary
{
    public class Chore : IChore
    {
        private ILogger _logger;
        private IMessageSender _messageSender;

        public string ChoreName { get; set; }
        public IPerson Owner { get; set; }
        public double HoursWorked { get; private set; }
        public bool IsComplete { get; private set; }

        public Chore(ILogger logger, IMessageSender messageSender)
        {
            _logger = logger;
            _messageSender = messageSender;
        }

        public void PerformedWork(double hours)
        {
            HoursWorked += hours;
            ////Logger log = new Logger(); ////<-- Tightly coupled to low-level models
            ////log.Log($"Performed work on { ChoreName }");
            _logger.Log($"Performed work on { ChoreName }");
        }
        public void CompleteChore()
        {
            IsComplete = true;

            /*
            Logger log = new Logger(); ////<-- Tightly coupled to low-level models
            log.Log($"Completed { ChoreName }");
            */
            _logger.Log($"Completed { ChoreName }");

            /*
            Emailer emailer = new Emailer(); ////<-- Tightly coupled to low-level models
            emailer.SendEmail(Owner, $"the chore { ChoreName } is complete.");
            */

            _messageSender.SendEmail(Owner, $"the chore { ChoreName } is complete.");
        }
    }
}
