﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Liskov_Library
{
    public interface IManager : IEmployee
    {
        void GeneratePerformanceReview();
    }
}
