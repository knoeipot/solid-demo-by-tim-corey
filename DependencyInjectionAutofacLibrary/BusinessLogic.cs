﻿using System;
using DependencyInjectionAutofacLibrary.Utilities;

namespace DependencyInjectionAutofacLibrary
{
    public class BusinessLogic : IBusinessLogic
    {
        ILogger _logger;
        IDataAccess _dataAccess;

        public BusinessLogic(ILogger logger, IDataAccess dataAccess)
        {
            _logger = logger;
            _dataAccess = dataAccess;
        }
        public void ProcessData()
        {

            /*
            Logger logger = new Logger();
            DataAccess dataAccess = new DataAccess();
            */
            //// Stop declaring these at low level

            /*
            logger.Log($"Starting the processing of data.");
            Console.WriteLine("Processing the data.");
            dataAccess.LoadData();
            dataAccess.SaveData("ProcessedInfo");
            logger.log("Finished processing of the data.");
            */

            _logger.Log($"Starting the processing of data.");
            Console.WriteLine("Processing the data.");
            _dataAccess.LoadData();
            _dataAccess.SaveData("ProcessedInfo");
            _logger.Log("Finished processing of the data.");

        }
    }
}
