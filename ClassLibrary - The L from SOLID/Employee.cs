﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary___The_L_from_SOLID
{
    public class Employee : BaseEmployee, iManaged
    {
        public IEmployee Manager { get; set; } = null;

        public virtual void AssignManager(IEmployee manager)
        {
            // Simulate ding other stuff here - otherwise, this should be
            // a property set statement, not a method
            Manager = manager;
        }
    }
}
