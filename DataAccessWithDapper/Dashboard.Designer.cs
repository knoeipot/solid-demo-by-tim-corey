﻿namespace DataAccessWithDapper
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchLastNameLabel = new System.Windows.Forms.Label();
            this.searchLastNameTextBox = new System.Windows.Forms.TextBox();
            this.searchLastNameButton = new System.Windows.Forms.Button();
            this.foundPeopleListBox = new System.Windows.Forms.ListBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.labelLastName = new System.Windows.Forms.Label();
            this.tbEmailAddress = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.tbPhoneNumber = new System.Windows.Forms.TextBox();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ButNewPerson = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // searchLastNameLabel
            // 
            this.searchLastNameLabel.AutoSize = true;
            this.searchLastNameLabel.Location = new System.Drawing.Point(12, 9);
            this.searchLastNameLabel.Name = "searchLastNameLabel";
            this.searchLastNameLabel.Size = new System.Drawing.Size(67, 15);
            this.searchLastNameLabel.TabIndex = 0;
            this.searchLastNameLabel.Text = "Last Name";
            // 
            // searchLastNameTextBox
            // 
            this.searchLastNameTextBox.Location = new System.Drawing.Point(104, 6);
            this.searchLastNameTextBox.Name = "searchLastNameTextBox";
            this.searchLastNameTextBox.Size = new System.Drawing.Size(168, 20);
            this.searchLastNameTextBox.TabIndex = 1;
            // 
            // searchLastNameButton
            // 
            this.searchLastNameButton.Location = new System.Drawing.Point(104, 32);
            this.searchLastNameButton.Name = "searchLastNameButton";
            this.searchLastNameButton.Size = new System.Drawing.Size(168, 23);
            this.searchLastNameButton.TabIndex = 2;
            this.searchLastNameButton.Text = "Search";
            this.searchLastNameButton.UseVisualStyleBackColor = true;
            this.searchLastNameButton.Click += new System.EventHandler(this.SearchLastNameButton_Click);
            // 
            // foundPeopleListBox
            // 
            this.foundPeopleListBox.FormattingEnabled = true;
            this.foundPeopleListBox.Location = new System.Drawing.Point(12, 61);
            this.foundPeopleListBox.Name = "foundPeopleListBox";
            this.foundPeopleListBox.Size = new System.Drawing.Size(260, 186);
            this.foundPeopleListBox.TabIndex = 3;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(104, 284);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(168, 20);
            this.tbFirstName.TabIndex = 5;
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(12, 287);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(67, 15);
            this.labelFirstName.TabIndex = 4;
            this.labelFirstName.Text = "First Name";
            // 
            // tbLastName
            // 
            this.tbLastName.Location = new System.Drawing.Point(104, 310);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(168, 20);
            this.tbLastName.TabIndex = 7;
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(13, 313);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(67, 15);
            this.labelLastName.TabIndex = 6;
            this.labelLastName.Text = "Last Name";
            // 
            // tbEmailAddress
            // 
            this.tbEmailAddress.Location = new System.Drawing.Point(104, 336);
            this.tbEmailAddress.Name = "tbEmailAddress";
            this.tbEmailAddress.Size = new System.Drawing.Size(168, 20);
            this.tbEmailAddress.TabIndex = 9;
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Location = new System.Drawing.Point(12, 339);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(86, 15);
            this.labelEmail.TabIndex = 8;
            this.labelEmail.Text = "Email Address";
            // 
            // tbPhoneNumber
            // 
            this.tbPhoneNumber.Location = new System.Drawing.Point(104, 362);
            this.tbPhoneNumber.Name = "tbPhoneNumber";
            this.tbPhoneNumber.Size = new System.Drawing.Size(168, 20);
            this.tbPhoneNumber.TabIndex = 11;
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Location = new System.Drawing.Point(12, 365);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(91, 15);
            this.labelPhoneNumber.TabIndex = 10;
            this.labelPhoneNumber.Text = "Phone Number";
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(104, 388);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(168, 20);
            this.textBox5.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(12, 391);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "Last Name";
            // 
            // ButNewPerson
            // 
            this.ButNewPerson.Location = new System.Drawing.Point(104, 414);
            this.ButNewPerson.Name = "ButNewPerson";
            this.ButNewPerson.Size = new System.Drawing.Size(169, 23);
            this.ButNewPerson.TabIndex = 14;
            this.ButNewPerson.Text = "Save Person";
            this.ButNewPerson.UseVisualStyleBackColor = true;
            this.ButNewPerson.Click += new System.EventHandler(this.ButNewPerson_Click);
            // 
            // Dashboard
            // 
            this.ClientSize = new System.Drawing.Size(285, 589);
            this.Controls.Add(this.ButNewPerson);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbPhoneNumber);
            this.Controls.Add(this.labelPhoneNumber);
            this.Controls.Add(this.tbEmailAddress);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.tbLastName);
            this.Controls.Add(this.labelLastName);
            this.Controls.Add(this.tbFirstName);
            this.Controls.Add(this.labelFirstName);
            this.Controls.Add(this.foundPeopleListBox);
            this.Controls.Add(this.searchLastNameButton);
            this.Controls.Add(this.searchLastNameTextBox);
            this.Controls.Add(this.searchLastNameLabel);
            this.Name = "Dashboard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        /*

        private System.Windows.Forms.ListBox PeopleFoundListBox;
        private System.Windows.Forms.TextBox LastNameText;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Label firstNameInsLabel;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lastNameInsLabel;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label phoneInsLabel;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label emailInsLabel;
        private System.Windows.Forms.TextBox textBox4;
        */
        private System.Windows.Forms.Label searchLastNameLabel;
        private System.Windows.Forms.TextBox searchLastNameTextBox;
        private System.Windows.Forms.Button searchLastNameButton;
        private System.Windows.Forms.ListBox foundPeopleListBox;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.Label labelFirstName;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.Label labelLastName;
        private System.Windows.Forms.TextBox tbEmailAddress;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.TextBox tbPhoneNumber;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ButNewPerson;
    }
}

