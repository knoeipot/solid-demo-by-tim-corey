﻿using System;
using DependencyInjectionAutofacLibrary;
using Autofac;

namespace ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            // We should get this instance from the container
            /*
            BusinessLogic businessLogic = new BusinessLogic();

            businessLogic.ProcessData();
            */
            var container = ContainerConfig.Configure();

            using (var scope = container.BeginLifetimeScope())
            {
                var app = scope.Resolve<IApplication>();

                app.Run();
            }
            //// Do the above, only at the start of the application


            //// VideoClip at 46:46


            Console.ReadLine();
        }
    }
}
