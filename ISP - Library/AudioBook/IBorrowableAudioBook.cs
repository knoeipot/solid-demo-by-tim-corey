﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISP___Library
{
    public interface IBorrowableAudioBook : IAudioBook, IBorrowable
    {
    }
}
