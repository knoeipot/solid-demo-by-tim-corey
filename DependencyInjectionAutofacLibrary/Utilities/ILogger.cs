﻿namespace DependencyInjectionAutofacLibrary.Utilities
{
    public interface ILogger
    {
        void Log(string message);
    }
}