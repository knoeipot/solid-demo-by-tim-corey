﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using DependencyInjectionAutofacLibrary;
using System.Reflection;
using System.Linq;

namespace ConsoleUI
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Application>().As<IApplication>();


            //// Not tightly coupled together. Dependency on interfaces not implementations
            ////

            builder.RegisterType<BetterBusinessLogic>().As<IBusinessLogic>();

            builder.RegisterAssemblyTypes(Assembly.Load(nameof(DependencyInjectionAutofacLibrary)))
                .Where(t => t.Namespace.Contains("Utilities"))
                .As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "I" + t.Name));

            return builder.Build();
        }
    }
}
