﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Console_UI___SOLID_03
{
    public class PersonDataCapture
    {
        public static Person Capture()
        {
            //Ask for user information
            Person output = new Person();

            Console.Write("What is your first name: ");
            output.FirstName = Console.ReadLine();

            Console.Write("What is your last name:");
            output.LastName = Console.ReadLine();

            return output;
        }
    }
}
