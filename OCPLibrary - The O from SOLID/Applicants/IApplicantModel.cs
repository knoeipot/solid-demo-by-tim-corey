﻿namespace OCPLibrary___The_O_from_SOLID
{
    public interface IApplicantModel
    {
        string FirstName { get; set; }
        string LastName { get; set; }

        IAccounts AccountProcessor { get; set; }
    }
}