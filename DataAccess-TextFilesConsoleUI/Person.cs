﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess_TextFilesConsoleUI
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string URL { get; set; }
    }
}
