﻿namespace DelegatesWinFormsUI
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.subTotalTextBox = new System.Windows.Forms.TextBox();
            this.lTotal = new System.Windows.Forms.Label();
            this.totalTextBox = new System.Windows.Forms.TextBox();
            this.butMessageBoxDemo = new System.Windows.Forms.Button();
            this.butTextBoxDemo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "Subtotal";
            // 
            // subTotalTextBox
            // 
            this.subTotalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subTotalTextBox.Location = new System.Drawing.Point(18, 48);
            this.subTotalTextBox.Name = "subTotalTextBox";
            this.subTotalTextBox.Size = new System.Drawing.Size(228, 41);
            this.subTotalTextBox.TabIndex = 1;
            // 
            // lTotal
            // 
            this.lTotal.AutoSize = true;
            this.lTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTotal.Location = new System.Drawing.Point(12, 102);
            this.lTotal.Name = "lTotal";
            this.lTotal.Size = new System.Drawing.Size(81, 36);
            this.lTotal.TabIndex = 3;
            this.lTotal.Text = "Total";
            // 
            // totalTextBox
            // 
            this.totalTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalTextBox.Location = new System.Drawing.Point(18, 141);
            this.totalTextBox.Name = "totalTextBox";
            this.totalTextBox.Size = new System.Drawing.Size(228, 41);
            this.totalTextBox.TabIndex = 4;
            // 
            // butMessageBoxDemo
            // 
            this.butMessageBoxDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butMessageBoxDemo.Location = new System.Drawing.Point(18, 188);
            this.butMessageBoxDemo.Name = "butMessageBoxDemo";
            this.butMessageBoxDemo.Size = new System.Drawing.Size(228, 51);
            this.butMessageBoxDemo.TabIndex = 5;
            this.butMessageBoxDemo.Text = "MessageBox Demo";
            this.butMessageBoxDemo.UseVisualStyleBackColor = true;
            this.butMessageBoxDemo.Click += new System.EventHandler(this.ButMessageBoxDemo_Click);
            // 
            // butTextBoxDemo
            // 
            this.butTextBoxDemo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butTextBoxDemo.Location = new System.Drawing.Point(252, 188);
            this.butTextBoxDemo.Name = "butTextBoxDemo";
            this.butTextBoxDemo.Size = new System.Drawing.Size(228, 51);
            this.butTextBoxDemo.TabIndex = 6;
            this.butTextBoxDemo.Text = "TextBox Demo";
            this.butTextBoxDemo.UseVisualStyleBackColor = true;
            this.butTextBoxDemo.Click += new System.EventHandler(this.ButTextBoxDemo_Click);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 256);
            this.Controls.Add(this.butTextBoxDemo);
            this.Controls.Add(this.butMessageBoxDemo);
            this.Controls.Add(this.totalTextBox);
            this.Controls.Add(this.lTotal);
            this.Controls.Add(this.subTotalTextBox);
            this.Controls.Add(this.label1);
            this.Name = "Dashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox subTotalTextBox;
        private System.Windows.Forms.Label lTotal;
        private System.Windows.Forms.TextBox totalTextBox;
        private System.Windows.Forms.Button butMessageBoxDemo;
        private System.Windows.Forms.Button butTextBoxDemo;
    }
}

