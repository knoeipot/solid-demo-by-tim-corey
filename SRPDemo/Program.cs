﻿namespace SRPDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ////Console.WriteLine("Welcome to my application!");
            StandardMessages.WelcomeMessage();

            /*
            //Ask for user information
            Person user = new Person();

            Console.Write("What is your first name: ");
            user.FirstName = Console.ReadLine();

            Console.Write("What is your last name:");
            user.LastName = Console.ReadLine();
            */

            Person user = PersonDataCapture.Capture();

            bool isUserValid = PersonValidator.Validate(user);

            if(isUserValid == false)
            {
                StandardMessages.EndApplication();
                return;
            }

            /*
            // Checks to be sure the first and last name are valid
            if(string.IsNullOrWhiteSpace(user.FirstName))
            {
                Console.WriteLine("You did not give us a valid first name");
                ////Console.ReadLine();
                StandardMessages.EndApplication();
                return;
            }

            if(string.IsNullOrWhiteSpace(user.LastName))
            {
                Console.WriteLine("You did not give us a valid last name");
                ////Console.ReadLine();
                StandardMessages.EndApplication();
                return;
            }
            */

            // Create a username for the person
            ////Console.WriteLine($"Your username is { user.FirstName.Substring(0,1)}{user.LastName}");

            AccountGenerator.CreateAccount(user);


            ////Console.ReadLine();
            StandardMessages.EndApplication();            
        }
    }
}
