﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary___The_L_from_SOLID
{
    public class Manager : Employee, IManager
    {
        //public string FirstName { get; set; }

        //public string LastName { get; set; }


        public override void CalculatePerHourRate(int rank)
        {
            decimal baseAmount = 19.75M;

            Salary = baseAmount + (rank * 4);
        }

        public void GeneratePerformanceReview()
        {
            Console.WriteLine("I'm receiving a direct report's performance.");
        }
    }
}
