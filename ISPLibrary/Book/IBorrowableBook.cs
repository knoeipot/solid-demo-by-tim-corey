﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISPLibrary
{
    public interface IBorrowableBook : IBorrowable, IBook
    {
    }
}
