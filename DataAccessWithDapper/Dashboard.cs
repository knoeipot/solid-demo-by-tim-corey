﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataAccessWithDapper
{
    public partial class Dashboard : Form
    {
        List<Person> people = new List<Person>();

        public Dashboard()
        {
            InitializeComponent();
            UpdateBinding();
        }

        private void UpdateBinding()
        {
            foundPeopleListBox.DataSource = people;
            foundPeopleListBox.DisplayMember = "FullInfo";
        }

        private void SearchLastNameButton_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            people = db.GetPeople(searchLastNameTextBox.Text);
            UpdateBinding();
        }

        private void ButNewPerson_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();

            db.InsertPerson(tbFirstName.Text, tbLastName.Text, tbEmailAddress.Text, tbPhoneNumber.Text);


            tbFirstName.Text = "";
            tbLastName.Text = "";
            tbEmailAddress.Text = "";
            tbPhoneNumber.Text = "";

                
        }
    }
}
