﻿using LSPLibrary;
using System;

namespace LSPConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            IManager accountingVP = new Manager();

            accountingVP.FirstName = "Emma";
            accountingVP.LastName = "stone";
            accountingVP.CalculatePerHourRate(4);

            IManaged emp = new Manager();
            ////Employee emp = new Manager(); //-> Works
            ////Employee emp = new CEO(); //-> Doesn't work


            emp.FirstName = "Tim";
            emp.LastName = "Corey";
            emp.AssignManager(accountingVP);
            emp.CalculatePerHourRate(2);

            Console.WriteLine($"{ emp.FirstName }'s salary is ${ emp.Salary }/hour");

            Console.ReadLine();
        }
    }
}
