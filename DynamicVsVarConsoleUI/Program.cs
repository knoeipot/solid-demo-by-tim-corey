﻿using System;

namespace DynamicVsVarConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            //// dynamic - use it for talking to other languages
            dynamic testDynamic = "";
            var testVar = "";

            var myItem = new { FirstName = "Tim", Email = "test@test.com" };

            Console.WriteLine($"Hello { myItem.FirstName }: your email is { myItem.Email }");

            Console.WriteLine(GetMessage());



            /*
            dynamic testDynamic = new Person();
            var testVar = new Person();

            testDynamic.FirstName = "Tim";
            testDynamic.LastName = "Corey";
            testDynamic.Email = "test@test.com";

            testVar.FirstName = "Sue";
            testVar.LastName = "Storm";

            testDynamic.SayHello();
            testVar.SayHello();

            testDynamic = "Hi";

            ////testVar = "Hi";

            Console.WriteLine(testDynamic);
            */
            /*
            dynamic testDynamic = "";//// dynamic == object
            
            var testVar = 2.1;//// knows type at runtime

            testVar = 1.1;

            testDynamic.SayHi();

            testDynamic = 1;//// int

            testDynamic = testDynamic + 2.1;//// Double!!

            Console.WriteLine(testDynamic);
            */
            Console.ReadLine();
        }

        static dynamic GetMessage()
        {
            return "This is a test";
        }
    }
}
