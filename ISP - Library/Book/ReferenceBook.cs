﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISP___Library
{
    public class ReferenceBook : IBook //: ILibraryItem
    {
        public string Author { get; set; }
        public string LibraryId { get; set; }
        public int Pages { get; set; }
        public string Title { get; set; }

    }
}
