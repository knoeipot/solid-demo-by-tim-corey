﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISPLibrary
{
    public class DVD : IBorrowableDVD
    {
        public string LibraryId { get; set; }
        ////public string Author { get; set; }
        public string Title { get; set; }
        public DateTime BorrowDate { get; set; }
        public string Borrower { get; set; }
        public int CheckOutDurationInDays { get; set; }
        ////public int Pages { get; set; } = -1; //// Doesn't apply
        public List<string> Actors { get; set; }
        public int RuntimeInMinutes { get; set; }

        public void CheckIn()
        {
            Borrower = string.Empty;
        }

        public void CheckOut(string borrower)
        {
            Borrower = borrower;
            BorrowDate = DateTime.Now;
        }

        public DateTime GetDueDate()
        {
            return BorrowDate.AddDays(CheckOutDurationInDays);
        }
    }
}
