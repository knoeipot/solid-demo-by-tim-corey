﻿using System;

namespace ISP___Library
{
    public interface ILibraryItem
    {
        string LibraryId { get; set; }

        string Title { get; set; }

    }
}
