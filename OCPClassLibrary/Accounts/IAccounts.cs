﻿namespace OCPClassLibrary
{
    public interface IAccounts
    {
        EmployeeModel Create(IApplicantModel person);
    }
}