﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISPLibrary
{
    public class ReferenceBook : IBook
    {
        public string LibraryId { get; set; }
        public int Pages { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        ////public DateTime BorrowDate { get; set; }
        ////public string Borrower { get; set; }
        ////public int CheckOutDurationInDays { get; set; } = 0;
        ////public void CheckIn()
        ////{
        ////    throw new NotImplementedException();
        ////}

        ////public void CheckOut(string borrower)
        ////{
        ////    throw new NotImplementedException();
        ////}

        ////public DateTime GetDueDate()
        ////{
        ////    throw new NotImplementedException();
        ////}
    }
}
