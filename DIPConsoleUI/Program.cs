﻿using DIPLibrary;
using System;

namespace DIPConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            //// Situation: High-level models depend on low-level models
            //// High-level models: Program, Chore - They depend on other classes/low-level models
            //// Low-level models: Logger, Emailer - Don't depend on other classes/models
            ////
            //// Goal this demo: The low-level models depend on high-level models
            //// (D.I.) #1 both should depend on abstractions (Interface)
            //// #2 Those abstractions should not depend on details

            ////Person owner = new Person
            /*
            IPerson owner = new Person ////<-- Tightly coupled to low-level models
            {
                FirstName = "Tim",
                LastName = "Corey",
                EmailAddress = "tim@iamtimcorey.com",
                PhoneNumber = "555-1212"
            };
            */
            IPerson owner = Factory.CreatePerson();
            owner.FirstName = "Tim";
            owner.LastName = "Corey";
            owner.EmailAddress = "tim@iamtimcorey.com";
            owner.PhoneNumber = "555-1212";

            /*
            Chore chore = new Chore ////<-- Tightly coupled to low-level models
            {
                ChoreName = "Take out the trash",
                Owner = owner
            };
            */

            IChore chore = Factory.CreateChore();
            chore.ChoreName = "Take out the trash";
            chore.Owner = owner;

            chore.PerformedWork(3);
            chore.PerformedWork(1.5);
            chore.CompleteChore();

            Console.ReadLine();
        }
    }
}
